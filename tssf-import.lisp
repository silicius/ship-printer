;;;; tssf-import.lisp

(in-package #:ship-printer)

;;; .pon files have the following structure:
;;; 
;;; |GAME
;;; |CARD-DECLARATION
;;; |...
;;;
;;; Where each CARD-DECLARATION is a record describing a card in the GAME.
;;; It consist of fields separated by the backtick ("`").
;;; The first field denotes the card type.
;;;
;;; These are the card types and their fields:
;;; START: art  | symbols     | name        | keywords    | description | flavour
;;; Pony:  art  | symbols     | name        | keywords    | description | flavour
;;; Ship:  art  | name        | description | flavour
;;; Goal:  art  | goal-points | name        | description | flavour
;;; Card:  name
;;; RulesX: RulesX
;;;
;;; The field types:
;;; art:         filename of art of the card
;;; symbols:     a list of symbols to be put on cards, separated by the exlamation mark ("!")
;;; name:        Name of the card
;;; keywords:    list of keywords of the card, separated by a comma (",")
;;; description: main description of the card, details its power.
;;; flavour:     Flavour text of the card, will be less visible than description.
;;; goal-points: is formatted as "goal!X" where X is a number denoting the point value of the goal card.
;;; RulesX:      X is a number that I do not know what means at this time. I believe it makes the rule cards to be included in the card set.
;;;

(defun \n-to-newline (string)
  (str:replace-all "\\n" (format nil "~%") string))



(defun .pon-record-line-to-list (line)
  "Transforms a record line from a .pon file into list."
  (flet ((convert-symbols (symbols)
	   (map 'list #'(lambda (x) (intern (str:upcase x) "KEYWORD")) (str:split "!" symbols)))
	 (convert-keywords (keywords)
	   (map 'list #'(lambda (x) (str:trim x)) (str:split "," keywords))))

    (match (str:split "`" line)
      ((list* "START" art symbols name keywords description flavour junk)
       (when junk (format *error-output* "Junk in ~S~%" line))
       (make-instance 'start-card
		      :title name
		      :art art
		      :symbols (convert-symbols symbols)
		      :keywords (convert-keywords keywords)
		      :description  (\n-to-newline description)
		      :flavour-text (\n-to-newline flavour)))

      ((list* "Pony" art symbols name keywords description flavour junk)
       (when junk (format *error-output* "Junk in ~S~%" line))
       (make-instance 'pony-card
		      :title name
		      :art art
		      :symbols (convert-symbols symbols)
		      :keywords (convert-keywords keywords)
		      :description  (\n-to-newline description)
		      :flavour-text (\n-to-newline flavour)))

      ((list* "Ship" art name description flavour _)
       (list 'ship art name (\n-to-newline description) (\n-to-newline flavour)))

      ((list* "Goal" art goal-points name _ description flavour _)
       (list 'goal art (nth 1 (str:split "!" goal-points))
	     name (\n-to-newline description) (\n-to-newline flavour)))
      
      ((list "Card" name)
       (list 'card name))
      
      ((list* unknown)
       (format *error-output* "Unknown card: ~S~&" unknown)))))

(defun .pon-to-lisp (filename)
  (with-open-file (pon-stream filename :direction :input)
    (let ((line (read-line pon-stream)))
      (unless (string= (str:trim line) "TSSSF")
	(error "TSSF expected at the start of the file!")))
    (loop as line = (read-line pon-stream nil)
	  while line
	  for record = (.pon-record-line-to-list line)
	  when record
	    do (format t "~S~%" record))))
