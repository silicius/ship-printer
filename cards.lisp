;;;; cards.lisp

(in-package #:ship-printer)


(defclass card ()
  ((art :initarg :art)
   (title :initarg :title)
   (description :initarg :description)
   (flavour-text :initarg :flavour-text)))

(defclass pony-card (card)
  ((keywords :initarg :keywords)
   (symbols :initarg :symbols)))

(defclass start-card (pony-card) ())
