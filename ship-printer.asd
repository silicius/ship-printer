;;;; ship-printer.asd

(asdf:defsystem #:ship-printer
  :description "TSSSF card generator inspired by CardMachine, with Polish translation."
  :author "silicius <silicius@schwi.pl>"
  :license  "to be filled"
  :version "0.0.1"
  :serial t
  :depends-on (#:opticl #:trivia #:str)
  :components ((:file "package")
	       (:file "cards")
	       (:file "tssf-import")
               (:file "ship-printer")))
